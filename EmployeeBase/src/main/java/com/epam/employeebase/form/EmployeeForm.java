package com.epam.employeebase.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import com.epam.employeebase.model.Employee;

@SuppressWarnings("serial")
public final class EmployeeForm extends ActionForm {
	
	private List<Employee> employees;
	private int entriesPerPage = 50;
	private int currentPage = 1;
	private static int employeeNumber;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public int getEntriesPerPage() {
		return entriesPerPage;
	}

	public void setEntriesPerPage(int entriesPerPage) {
		this.entriesPerPage = entriesPerPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		EmployeeForm.employeeNumber = employeeNumber;
	}
}
