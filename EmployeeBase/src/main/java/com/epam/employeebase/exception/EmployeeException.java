package com.epam.employeebase.exception;

public final class EmployeeException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public EmployeeException(String msg) {
		super(msg);
	}
	
	public EmployeeException(Exception ex) {
		super(ex);
	}
}
