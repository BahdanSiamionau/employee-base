package com.epam.employeebase.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.epam.employeebase.dao.HibernateDAO;
import com.epam.employeebase.dao.IDAO;
import com.epam.employeebase.exception.EmployeeException;
import com.epam.employeebase.form.EmployeeForm;
import com.epam.employeebase.resource.EmployeeConstant;
import com.epam.employeebase.view.tag.PaginationTag;
import com.epam.employeebase.view.tag.PaginationTag.RequestProcessingResult;

public final class EmployeeAction extends DispatchAction {
	
	private IDAO dao = new HibernateDAO();
	private static final Log LOG = LogFactory.getLog(EmployeeAction.class);

	public ActionForward index(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		 
		EmployeeForm employeeForm = (EmployeeForm) form;
		try {
			employeeForm.setEmployeeNumber(dao.countEmployees());
			RequestProcessingResult result = PaginationTag.processRequest(request,
					employeeForm.getCurrentPage(), employeeForm.getEntriesPerPage(),
					employeeForm.getEmployeeNumber());
			employeeForm.setEmployees(
					dao.getEmployeeList(result.getFromEntry(), result.getToEntry()));
			employeeForm.setCurrentPage(result.getNewCurrentPage());
			employeeForm.setEntriesPerPage(result.getNewEntriesPerPage());
		} catch (EmployeeException ex) {
			LOG.error(ex.getMessage());
		}
		return mapping.findForward(EmployeeConstant.INDEX);
	}
}