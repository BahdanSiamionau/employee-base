package com.epam.employeebase.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.employeebase.model.Employee;
import com.epam.employeebase.resource.EmployeeConstant;

public final class JPADAO implements IDAO {
	
	private EntityManagerFactory entityManagerFactory;
	private static final String PU_NAME = "employee-persistence-unit";
	private static final Log LOG = LogFactory.getLog(JPADAO.class);
	
	public JPADAO() {
		try {
			entityManagerFactory = Persistence
					.createEntityManagerFactory(PU_NAME);
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
	    	throw new ExceptionInInitializerError(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployeeList(Integer fromEntry, Integer toEntry) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		Query query = entityManager.createNamedQuery(EmployeeConstant.EMPLOYEES_QUERY);
		System.out.println(fromEntry);
		query.setFirstResult(fromEntry);
		query.setMaxResults(toEntry - fromEntry);
		List<Employee> list = query.getResultList();
		entityManager.getTransaction().commit();
		return list;
	}

	@Override
	public Integer countEmployees() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		Query query = entityManager.createNativeQuery(EmployeeConstant.SQL_COUNT_EMPLOYEES);
		Integer employeesNumber = ((BigDecimal)query.getSingleResult()).intValue();
		entityManager.getTransaction().commit();
		return employeesNumber;
	}
}
