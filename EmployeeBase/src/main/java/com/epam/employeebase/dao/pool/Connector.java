package com.epam.employeebase.dao.pool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.employeebase.exception.EmployeeException;

public final class Connector implements AutoCloseable {
	
	private ConnectorPool pool;
	private Connection connection;
	private static final String EXCEPTION_NULL_CONN = "Connection is null";
	private static final Log LOG = LogFactory.getLog(Connector.class);

	public Connector(ConnectorPool pool, Connection connection) {
		this.pool = pool;
		this.connection = connection;
	}
	
	public Statement getStatement() throws EmployeeException {
		try {
			if (connection != null) {
				return connection.createStatement();
			}
			throw new EmployeeException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			LOG.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}
	
	public PreparedStatement getPreparedStatement(String query) throws EmployeeException {
		try {
			if (connection != null) {
				return connection.prepareStatement(query);
			}
			throw new EmployeeException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			LOG.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}
	
	@Override
	public void close() throws EmployeeException {
		try {
			pool.release(this);
		} catch (EmployeeException ex) {
			LOG.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}
}