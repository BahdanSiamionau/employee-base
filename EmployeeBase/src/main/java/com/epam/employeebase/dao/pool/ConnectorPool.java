package com.epam.employeebase.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.employeebase.exception.EmployeeException;
import com.epam.employeebase.resource.EmployeeBundle;

public final class ConnectorPool {

	private ArrayBlockingQueue<Connector> connectors;
	private static final int MAX_CONNECTORS = 10;	
	private static final Log log = LogFactory.getLog(ConnectorPool.class);
	private static final String DRIVER_NAME = "driver_name";
	private static final String URL = "url";
	private static final String USER = "user";
	private static final String PASSWORD = "password";
	
	private static final class InstanceHolder {
		private static ConnectorPool INSTANCE; 
		static {
			try {
				INSTANCE = new ConnectorPool();
			} catch (EmployeeException ex) {
				log.error(ex.getMessage());
				throw new ExceptionInInitializerError(ex);
			}
		}
	}
	
	public static ConnectorPool getInstance() {
		return InstanceHolder.INSTANCE;
	}
	
	private ConnectorPool() throws EmployeeException {
		try {
			Class.forName(EmployeeBundle.getString(DRIVER_NAME));
			connectors = new ArrayBlockingQueue<Connector>(MAX_CONNECTORS);
			for (int i = 0; i < MAX_CONNECTORS; i++) {
				Connection connection = DriverManager.getConnection(
						EmployeeBundle.getString(URL),
						EmployeeBundle.getString(USER),
						EmployeeBundle.getString(PASSWORD));
				Connector connector = new Connector(this, connection);
				connectors.put(connector);
			}
		} catch (SQLException | ClassNotFoundException | InterruptedException ex) {
			log.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}

	public Connector take() throws EmployeeException {
		try {
			Connector connector = connectors.take();
			return connector;
		} catch (InterruptedException ex) {
			log.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}

	public void release(Connector connector) throws EmployeeException {
		try {
			connectors.put(connector);
		} catch (InterruptedException ex) {
			log.error(ex.getMessage());
			throw new EmployeeException(ex);
		}
	}
}
