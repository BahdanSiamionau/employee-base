package com.epam.employeebase.dao;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.epam.employeebase.model.Employee;
import com.epam.employeebase.resource.EmployeeConstant;

public final class HibernateDAO implements IDAO {
	
	private SessionFactory sessionFactory;
	private static final String CFG_PATH = "src/main/webapp/WEB-INF/hibernate.cfg.xml";
	private static final Log LOG = LogFactory.getLog(HibernateDAO.class);
	
	public HibernateDAO() {
		try {
			sessionFactory = new Configuration()
				.configure(new File(CFG_PATH))
				.buildSessionFactory();
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
			throw new ExceptionInInitializerError(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployeeList(Integer fromEntry, Integer toEntry) {
		Session session;
		session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		List<Employee> list = session.getNamedQuery(EmployeeConstant.EMPLOYEES_QUERY)
				.setFirstResult(fromEntry)
				.setMaxResults(toEntry - fromEntry)
				.list();
		session.getTransaction().commit();
		return list;
	}

	@Override
	public Integer countEmployees() {
		Session session;
		session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery(EmployeeConstant.SQL_COUNT_EMPLOYEES);
		Integer employeesNumber = ((BigDecimal)query.uniqueResult()).intValue();
		session.getTransaction().commit();
		return employeesNumber;
	}
}
