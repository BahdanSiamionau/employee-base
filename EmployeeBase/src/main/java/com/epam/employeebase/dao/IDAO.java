package com.epam.employeebase.dao;

import java.util.List;

import com.epam.employeebase.exception.EmployeeException;
import com.epam.employeebase.model.Employee;

public interface IDAO {

	public List<Employee> getEmployeeList(Integer fromEntry, Integer toEntry);
	public Integer countEmployees() throws EmployeeException;
}
