package com.epam.employeebase.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.epam.employeebase.dao.pool.Connector;
import com.epam.employeebase.dao.pool.ConnectorPool;
import com.epam.employeebase.exception.EmployeeException;
import com.epam.employeebase.model.Address;
import com.epam.employeebase.model.City;
import com.epam.employeebase.model.Company;
import com.epam.employeebase.model.Country;
import com.epam.employeebase.model.Employee;
import com.epam.employeebase.model.Employment;
import com.epam.employeebase.model.Office;
import com.epam.employeebase.model.Position;
import com.epam.employeebase.resource.EmployeeConstant;

public final class JDBCDAO implements IDAO {

	private static final Log LOG = LogFactory.getLog(JDBCDAO.class);
	private static final String EMPLOYEES_NUMBER_EXCEPTION = "Could not get employees number";
	private static String EMPLOYEE_QUERY;
	static {
	    BufferedReader br = null;
	    try {
	    	br = new BufferedReader(
	    			new FileReader("src/main/webapp/sql/employees.sql"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            EMPLOYEE_QUERY = sb.toString();
	    } catch(IOException ex){
	    	LOG.error(ex.getMessage());
	    	throw new ExceptionInInitializerError(ex);
	    } finally{
	    	JDBCDAO.closeQuietly(br);
	    }
	}
	
	@Override
	public List<Employee> getEmployeeList(Integer fromEntry, Integer toEntry) {
		List<Employee> employees = new ArrayList<Employee>();
		try (Connector conn = ConnectorPool.getInstance().take()) {
			String completeQuery = String.format(EMPLOYEE_QUERY, toEntry + 1, fromEntry + 1);
			ResultSet rs = conn.getStatement()
					.executeQuery(completeQuery);
			while (rs.next()) {
				Employee employee = containsId(employees, rs.getLong(EmployeeConstant.EMPLOYEE_ID));
				if (employee == null) {
					Country employeeCountry = new Country();
					Country officeCountry = new Country();
					City employeeCity = new City();
					City officeCity = new City();
					Address employeeAddress = new Address();
					Address officeAddress = new Address();
					employee = new Employee();
					Employment employment = new Employment();
					Office office = new Office();
					Company company = new Company();
					Position position = new Position();
					
					List<Employment> employments = new ArrayList<Employment>();
					employee.setEmployments(employments);			
					
					employeeCountry.setName(rs.getString(EmployeeConstant.EMPLOYEE_COUNTRY));
					officeCountry.setName(rs.getString(EmployeeConstant.OFFICE_COUNTRY));
					employeeCity.setName(rs.getString(EmployeeConstant.EMPLOYEE_CITY));
					officeCity.setName(rs.getString(EmployeeConstant.OFFICE_CITY));
					employeeAddress.setStreet(rs.getString(EmployeeConstant.EMPLOYEE_ADDRESS));
					officeAddress.setStreet(rs.getString(EmployeeConstant.OFFICE_ADDRESS));
					employee.setId(rs.getLong(EmployeeConstant.EMPLOYEE_ID));
					employee.setFirstName(rs.getString(EmployeeConstant.FIRST_NAME));
					employee.setLastName(rs.getString(EmployeeConstant.LAST_NAME));
					position.setName(rs.getString(EmployeeConstant.POSITION));
					employment.setPosition(position);
					office.setEmployeesNumber(rs.getLong(EmployeeConstant.NUMBER_OF_EMPLOYEES));
					company.setName(rs.getString(EmployeeConstant.COMPANY));

					employeeCity.setCountry(employeeCountry);
					employeeAddress.setCity(employeeCity);
					employee.setAddress(employeeAddress);
					officeCity.setCountry(officeCountry);
					officeAddress.setCity(officeCity);
					office.setAddress(officeAddress);
					office.setCompany(company);
					employment.setOffice(office);
					employments.add(employment);
					
					employees.add(employee);
				} else {
					Country officeCountry = new Country();
					City officeCity = new City();
					Address officeAddress = new Address();
					Employment employment = new Employment();
					Office office = new Office();
					Company company = new Company();
					Position position = new Position();
					
					List<Employment> employments = employee.getEmployments();
					
					officeCountry.setName(rs.getString(EmployeeConstant.OFFICE_COUNTRY));
					officeCity.setName(rs.getString(EmployeeConstant.OFFICE_CITY));
					officeAddress.setStreet(rs.getString(EmployeeConstant.OFFICE_ADDRESS));
					position.setName(rs.getString(EmployeeConstant.POSITION));
					employment.setPosition(position);
					office.setEmployeesNumber(rs.getLong(EmployeeConstant.NUMBER_OF_EMPLOYEES));
					company.setName(rs.getString(EmployeeConstant.COMPANY));
					
					officeCity.setCountry(officeCountry);
					officeAddress.setCity(officeCity);
					office.setAddress(officeAddress);
					office.setCompany(company);
					employment.setOffice(office);
					employments.add(employment);
				}
			}
		} catch (EmployeeException | SQLException ex) {
			LOG.error(ex.getMessage());
			throw new ExceptionInInitializerError(ex);
		}
		return employees;
	}
	
	private Employee containsId(List<Employee> employees, Long id) {
		for (Employee employee : employees) {
			if (employee.getId().equals(id)) {
				return employee;
			}
		}
		return null;
	}
	
	private static void closeQuietly(Reader reader) {
		try {
			reader.close();
		} catch (IOException ex) {
			LOG.error(ex.getMessage());
		}
	}

	@Override
	public Integer countEmployees() throws EmployeeException {
		try (Connector conn = ConnectorPool.getInstance().take()) {
			ResultSet rs = conn.getStatement()
					.executeQuery(EmployeeConstant.SQL_COUNT_EMPLOYEES);
			while (rs.next()) {
				return rs.getInt(EmployeeConstant.EMPLOYEE_NUMBER);
			}
		} catch (EmployeeException | SQLException ex) {
			LOG.error(ex.getMessage());
			
		}
		throw new EmployeeException(EMPLOYEES_NUMBER_EXCEPTION);
	}
}
