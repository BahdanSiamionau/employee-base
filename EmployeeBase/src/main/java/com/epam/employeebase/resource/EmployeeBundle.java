package com.epam.employeebase.resource;

import java.util.ResourceBundle;

public final class EmployeeBundle {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(EmployeeConstant.DB_PROP_PATH);
	
	private EmployeeBundle() {
		
	}
	
	public static String getString(String key) {
		return BUNDLE.getString(key);
	}
}