package com.epam.employeebase.resource;

public final class EmployeeConstant {

	public static final String DB_PROP_PATH = "database";
	public static final String SQL_COUNT_EMPLOYEES = "select count(*) as employeeNumber from EMPLOYEE";
	public static final String INDEX_URL = "/EmployeeAction.do?method=index";
	
	public static final String INDEX = "index";
	public static final String PAGE = "page";
	public static final String NEXT = "next";
	public static final String PREV = "prev";
	public static final String PREVIOUS = "previous";
	
	public static final String EMPLOYEE_NUMBER = "employeeNumber";
	public static final String EMPLOYEE_ID = "EMPLOYEE_ID";
	public static final String EMPLOYEE_COUNTRY = "EMPLOYEE_COUNTRY";
	public static final String OFFICE_COUNTRY = "OFFICE_COUNTRY";
	public static final String EMPLOYEE_CITY = "EMPLOYEE_CITY";
	public static final String OFFICE_CITY = "OFFICE_CITY";
	public static final String EMPLOYEE_ADDRESS = "EMPLOYEE_ADDRESS";
	public static final String OFFICE_ADDRESS = "OFFICE_ADDRESS";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final String POSITION = "POSITION";
	public static final String NUMBER_OF_EMPLOYEES = "NUMBER_OF_EMPLOYEES";
	public static final String COMPANY = "COMPANY";
	
	public static final String EMPLOYEES_QUERY = "EMPLOYEES_QUERY";
	public static final String EMPLOYEE_FORM = "employeeForm";
	
	private EmployeeConstant() {
		
	}
}
