package com.epam.employeebase.view.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class PaginationTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private static final Log LOG = LogFactory.getLog(PaginationTag.class);
	private int currentPage;
	private int entriesPerPage;
	private int employeeNumber;
	private String applicationName;
	private String controllerName;
	private String methodName;
	
	public static final class RequestProcessingResult {
		
		private int fromEntry;
		private int toEntry;
		private int newCurrentPage;
		private int newEntriesPerPage;
		
		public RequestProcessingResult(int fromEntry, int toEntry,
					int newCurrentPage, int newEntriesPerPage) {
			this.fromEntry = fromEntry;
			this.toEntry = toEntry;
			this.newCurrentPage = newCurrentPage;
			this.newEntriesPerPage = newEntriesPerPage;
		}

		public int getFromEntry() {
			return fromEntry;
		}

		public void setFromEntry(int fromEntry) {
			this.fromEntry = fromEntry;
		}

		public int getToEntry() {
			return toEntry;
		}

		public void setToEntry(int toEntry) {
			this.toEntry = toEntry;
		}

		public int getNewCurrentPage() {
			return newCurrentPage;
		}

		public void setNewCurrentPage(int newCurrentPage) {
			this.newCurrentPage = newCurrentPage;
		}

		public int getNewEntriesPerPage() {
			return newEntriesPerPage;
		}

		public void setNewEntriesPerPage(int newEntriesPerPage) {
			this.newEntriesPerPage = newEntriesPerPage;
		}
	}
	
	public static RequestProcessingResult processRequest(HttpServletRequest request,
				int currentPage, int entriesPerPage, int employeeNumber) {
		int fromEntry, toEntry, newCurrentPage, newEntriesPerPage;
		fromEntry = entriesPerPage*(currentPage - 1);
		toEntry = fromEntry + entriesPerPage;
		newCurrentPage = currentPage;
		newEntriesPerPage = entriesPerPage;
		String formName = request.getParameter("form");
		if (formName != null) {
			switch (formName) {
			case "page":
				newCurrentPage = Integer.parseInt(request.getParameter("page"));
				int lastPage = employeeNumber/newEntriesPerPage + 1;
				if (newCurrentPage > lastPage) {
					newCurrentPage = lastPage;
				} 
				fromEntry = entriesPerPage*(newCurrentPage - 1);
				toEntry = fromEntry + entriesPerPage;
				break;
			case "entries":
				newCurrentPage = 1;
				newEntriesPerPage = Integer.parseInt(request.getParameter("pp"));
				fromEntry = newEntriesPerPage*(newCurrentPage - 1);
				toEntry = fromEntry + newEntriesPerPage;
				break;
			}
		} 
		return new RequestProcessingResult(fromEntry, toEntry,
				newCurrentPage, newEntriesPerPage);
	}
	
	public int doStartTag() throws JspException {
		StringBuilder content = new StringBuilder();
		int lastPage = employeeNumber/entriesPerPage + ((employeeNumber % entriesPerPage == 0) ? 0 : 1);
		boolean isFirstPage, isLastPage;
		isFirstPage = currentPage == 1;
		isLastPage = currentPage == lastPage;
		try {
			if (pageContext.getAttribute("validated") == null) {
				content.append("<script type='text/javascript'>");
				content.append("function pp_handler(form) {");
				content.append("var re = /^[0-9]+$/;");
				content.append("var text = form.pp;");
				content.append("if (!re.test(text.value)) {");
				content.append("alert('Field should contain only digits!');");
				content.append("return false;");
				content.append("} else if (text.value < 1) {");
				content.append("alert('At least 1 entry per page available!');");
				content.append("return false;");
				content.append("} else if (text.value > 200) {");
				content.append("alert('Maximum 200 entries per page available!');");
				content.append("return false;}");
				content.append("return true;}");
				content.append("function gt_handler(form) {");
				content.append("var re = /^[0-9]+$/;");
				content.append("var text = form.page;");
				content.append("if (!re.test(text.value)) {");
				content.append("alert('Field should contain only digits!');");
				content.append("return false;");
				content.append("} else if (text.value < 1) {");
				content.append("return false;}");
				content.append("return true;}");
				content.append("</script>");
				pageContext.setAttribute("validated", true);
			}
			content.append("<div style='text-align: center;'>");
			content.append("<p>");
			
            if (!isFirstPage) {
            	appendNextPrevForm(content, true, currentPage);
            }
            appendPageForm(content, 1, currentPage == 1);
            appendSeparator(content, currentPage > 4);
            for (int i = currentPage - 2; i <= currentPage + 2; i++) {
            	if (i > 1 && i < lastPage) {
            		appendPageForm(content, i, currentPage == i);
            	}
            }
            appendSeparator(content, currentPage < lastPage - 3);
            appendPageForm(content, lastPage, currentPage == lastPage);
            if (!isLastPage) {
            	appendNextPrevForm(content, false, currentPage);
            } 
            content.append(String.format("<form action='/%s/%s' style='display: inline;' onsubmit='return pp_handler(this);'>", applicationName, controllerName));
            content.append(String.format("<input type='hidden' name='%s' value='index'/>", methodName));
            content.append("<input type='hidden' name='form' value='entries'/>");
            content.append("<input type='text' name='pp' id='pp'/>");
            content.append("<input type='submit' value='entries per page'/>");
            content.append("</form>");
            content.append(String.format("<form action='/%s/%s' style='display: inline;' onsubmit='return gt_handler(this);'>", applicationName, controllerName));
            content.append(String.format("<input type='hidden' name='%s' value='index'/>", methodName));
            content.append("<input type='hidden' name='form' value='page'/>");
            content.append("<input type='text' name='page' id='gt'/>");
            content.append("<input type='submit' value='go to page'/>");
            content.append("</form>");
            content.append("</p>");
            content.append("</div>");
            pageContext.getOut().write(content.toString());
			
		} catch (IOException ex) {
			LOG.error(ex.getMessage());
		}
		return SKIP_BODY;
	}
	
	private void appendNextPrevForm(StringBuilder content, boolean isPrev, int currentPage) {
		content.append(String.format("<form action='/%s/%s' style='display: inline;'>", applicationName, controllerName));
		content.append(String.format("<input type='hidden' name='%s' value='index'/>", methodName));
		content.append("<input type='hidden' name='form' value='page'/>");
		content.append(String.format("<input type='hidden' name='page' value='%s'/>", isPrev ? currentPage - 1 : currentPage + 1));
		content.append(String.format("<input type='submit' value='%s'/>", isPrev ? "prev" : "next"));
		content.append("</form>");
	}
	
	private void appendPageForm(StringBuilder content, int page, boolean isCurrent) {
		String isCurrentStyle = "style='border-color: red;'";
		content.append(String.format("<form action='/%s/%s' style='display: inline;'>", applicationName, controllerName));
		content.append(String.format("<input type='hidden' name='%s' value='index'/>", methodName));
		content.append("<input type='hidden' name='form' value='page'/>");
		content.append(String.format("<input type='hidden' name='page' value='%s'/>", page));
		content.append(String.format("<input %s type='submit' value='%s'/>", isCurrent ? isCurrentStyle : "", page));
		content.append("</form>");
	}
	
	private void appendSeparator(StringBuilder content, boolean isRequired) {
		if(isRequired) { 
			content.append("...");
		}
	}
	
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = Integer.parseInt(currentPage);
	}

	public void setEntriesPerPage(String entriesPerPage) {
		this.entriesPerPage = Integer.parseInt(entriesPerPage);
	}
	
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = Integer.parseInt(employeeNumber);
	}
}
