package com.epam.employeebase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="POSITION")
public final class Position {
	
	private Long id;
	private String name;
	
	@Id
	@SequenceGenerator(name="ID", sequenceName="SEQ_POSITION")
	@GeneratedValue(generator="ID")
	@Column(name = "ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="NAME")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
