package com.epam.employeebase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;

@Entity
@Table(name="OFFICE")
public final class Office {

	private Long id;
	private Company company;
	private Address address;
	private Long employeesNumber;
	
	@Id
	@SequenceGenerator(name="ID", sequenceName="SEQ_OFFICE")
	@GeneratedValue(generator="ID")
	@Column(name = "ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	public Company getCompany() {
		return company;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	@Formula(value="(select count(distinct EMPLOYEE.id) from student.EMPLOYEE, student.EMPLOYMENT where EMPLOYMENT.employee_Id = EMPLOYEE.id and EMPLOYMENT.office_id = id)")
	public Long getEmployeesNumber() {
		return employeesNumber;
	}
	
	public void setEmployeesNumber(Long employeesNumber) {
		this.employeesNumber = employeesNumber;
	}
}