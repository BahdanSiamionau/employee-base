<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="../tld/custom-tags.tld" prefix="tags"%>
<html>
<head>
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
</head>
<body>
	<nested:root name="employeeForm">
		<tags:pagination currentPage="${employeeForm.currentPage}" entriesPerPage="${employeeForm.entriesPerPage}" employeeNumber="${employeeForm.employeeNumber}" applicationName="employeebase" controllerName="EmployeeAction.do" methodName="method"/>
		<div class="cellsBlock">
	        <div class="cellSmall" style="background: #b0bca7;"> № </div>
	        <div class="cellMedium" style="background: #b0bca7;">First name</div>
	        <div class="cellMedium" style="background: #b0bca7;">Last name</div>
	        <div class="cellMedium" style="background: #b0bca7;">Employee country</div>
	        <div class="cellMedium" style="background: #b0bca7;">Employee city</div>
	        <div class="cellLarge" style="background: #b0bca7;">Employee address</div>
	        <div class="cellMedium" style="background: #b0bca7;">Company</div>
	        <div class="cellMedium" style="background: #b0bca7;">Office country</div>
	        <div class="cellMedium" style="background: #b0bca7;">Office city</div>
	        <div class="cellLarge" style="background: #b0bca7;">Office address</div>
	        <div class="cellSmall" style="background: #b0bca7;">n</div>
	        <div class="cellMedium" style="background: #b0bca7;">Position in office</div>
	    	<bean:parameter id="row_id" name="row_id" 
	    			value="${employeeForm.entriesPerPage*(employeeForm.currentPage - 1) + 1}"/>
	    	<nested:iterate property="employees">
	    		<nested:size id="employments_number" property="employments"/>
	    		<div class="cellSmall" style="height: ${employments_number * 50};">
	    			${row_id}
	    		</div>
	    		<div class="cellMedium" style="height: ${employments_number * 50};">
	    			<nested:write property="firstName"/>
	    		</div>
	    		<div class="cellMedium" style="height: ${employments_number * 50};">
	    			<nested:write property="lastName"/>
	    		</div>
	    		<nested:nest property="address">
	    			<nested:nest property="city">
	    				<div class="cellMedium" style="height: ${employments_number * 50};">
	    					<nested:write property="country.name"/>
	    				</div>
	    				<div class="cellMedium" style="height: ${employments_number * 50};">
		    				<nested:write property="name"/>
		    			</div>
	    			</nested:nest>
	    			<div class="cellLarge" style="height: ${employments_number * 50};">
	    				<nested:write property="street"/>
	    			</div>
	   			</nested:nest>
	    		<nested:iterate property="employments">
	    			<nested:nest property="office">
	    				<div class="cellMedium">
		   					<nested:write property="company.name"/>
		   				</div>
	    				<nested:nest property="address">
	    					<nested:nest property="city">
			    				<div class="cellMedium">
			    					<nested:write property="country.name"/>
			    				</div>
			    				<div class="cellMedium">
				    				<nested:write property="name"/>
				    			</div>
			    			</nested:nest>
			    			<div class="cellLarge">
			    				<nested:write property="street"/>
			    			</div>
	    				</nested:nest>
	    				<div class="cellSmall">
	    					<nested:define id="number" property="employeesNumber"/>
	    					${number}
		    			</div>
	    			</nested:nest>
	    			<nested:nest property="position">
	    				<div class="cellMedium">
		    				<nested:write property="name"/>
		    			</div>
	    			</nested:nest>
	    		</nested:iterate>
	    		<bean:parameter name="row_id" id="row_id" value="${row_id + 1}"/>
	    	</nested:iterate>
    	</div>
    	<tags:pagination currentPage="${employeeForm.currentPage}" entriesPerPage="${employeeForm.entriesPerPage}" employeeNumber="${employeeForm.employeeNumber}" applicationName="employeebase" controllerName="EmployeeAction.do" methodName="method"/>
    </nested:root>
</body>
</html>