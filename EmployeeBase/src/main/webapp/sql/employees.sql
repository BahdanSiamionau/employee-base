select
  ID_ as EMPLOYEE_ID,
  FIRST_NAME_ as FIRST_NAME,
  LAST_NAME_ as LAST_NAME,
  EMPLOYEE_COUNTRY_NAME as EMPLOYEE_COUNTRY,
  EMPLOYEE_CITY_NAME as EMPLOYEE_CITY,
  EMPLOYEE_ADDRESS_STREET as EMPLOYEE_ADDRESS,
  CM.NAME as COMPANY,
  CN.NAME as OFFICE_COUNTRY, 
  CT.NAME as OFFICE_CITY,
  AD.STREET as OFFICE_ADDRESS,
  NUMBER_OF_EMPLOYEES,
  PT.NAME as POSITION
from
  (
    select
      ID_,
      FIRST_NAME_, 
      LAST_NAME_,
      CN.NAME as EMPLOYEE_COUNTRY_NAME,
      CT.NAME as EMPLOYEE_CITY_NAME,
      AD.STREET as EMPLOYEE_ADDRESS_STREET
    from 
      ADDRESS AD, 
      CITY CT, 
      COUNTRY CN,
      (
        select
          ID_,
          FIRST_NAME_, 
          LAST_NAME_,
          ADDRESS_ID_
        from
          (
            select
              EM.ID as ID_,
              EM.FIRST_NAME as FIRST_NAME_, 
              EM.LAST_NAME as LAST_NAME_,
              EM.ADDRESS_ID as ADDRESS_ID_,
              row_number() over (order by ID) as RN
            from
              EMPLOYEE EM
            where 
              rownum < %s
          )
        where
          RN >= %s
      )
    where
      ADDRESS_ID_ = AD.ID
      and AD.CITY_ID = CT.ID
      and CT.COUNTRY_ID = CN.ID
  )
  inner join EMPLOYMENT EN
  on ID_ = EN.EMPLOYEE_ID, 
  (
    select
      OC.ID as OFFICE_ID_,
      count(EN.ID) as NUMBER_OF_EMPLOYEES
    from 
      EMPLOYMENT EN, 
      OFFICE OC
    where
      OC.ID = EN.OFFICE_ID
    group by
      OC.ID
  ),
  OFFICE OC, 
  COMPANY CM,
  ADDRESS AD, 
  CITY CT, 
  COUNTRY CN,
  POSITION PT
where
  EN.OFFICE_ID = OC.ID
  and OC.COMPANY_ID = CM.ID
  and OC.ADDRESS_ID = AD.ID
  and AD.CITY_ID = CT.ID
  and CT.COUNTRY_ID = CN.ID
  and OFFICE_ID_ = EN.OFFICE_ID
  and EN.POSITION_ID = PT.ID